#!/bin/sh

# cp -av /usr/local/apps/mdrenderer/mdrenderer.service /etc/systemd/system/mdrenderer.service
# systemctl enable mdrenderer.service
# systemctl start mdrenderer.service
# systemctl restart mdrenderer.service
# systemctl status mdrenderer.service

# firewall-cmd --zone=public --add-port=8002/tcp
# firewall-cmd --reload

/usr/local/apps/mdrenderer/markdown-renderer -mode local -dataserver http://xxx.example.com:8002 -root /home/http/public_html/docs
