module bitbucket.org/8ox86/markdown-renderer

go 1.12

require (
	github.com/shurcooL/github_flavored_markdown v0.0.0-20181002035957-2122de532470
	github.com/tomasen/realip v0.0.0-20180522021738-f0c99a92ddce
	go.uber.org/zap v1.12.0
)

replace github.com/shurcooL/github_flavored_markdown => github.com/ttys3/github_flavored_markdown v0.0.0-20191101040046-478ed05f7142
