package main

import (
	"bufio"
	"flag"
	"fmt"
	"github.com/shurcooL/github_flavored_markdown"
	"github.com/shurcooL/github_flavored_markdown/gfmstyle"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"regexp"
	"strings"
	"github.com/tomasen/realip"
)

var addr = flag.String("addr", "127.0.0.1:8002", "address to listen on")
var mode = flag.String("mode", "local", "local mode or http mode")
var dataServerUrl = flag.String("dataserver", "http://localhost:8003/", "the dataServerUrl server")
var dataRoot = flag.String("root", "/var/www/markdown", "the data root dir")

var validPath = regexp.MustCompile(`^/([/,.:，。：_\-a-zA-Z0-9\x{4e00}-\x{9fa5}]+)\.(md|markdown)$`)

var Version = "dev"

func init() {
	initLogger()
}

func initLogger() {
	config := zap.Config{
		Level:    zap.NewAtomicLevelAt(zap.InfoLevel),
		Encoding: "console",
		EncoderConfig: zapcore.EncoderConfig{
			TimeKey:        "ts",
			LevelKey:       "level",
			MessageKey:     "msg",
			LineEnding:     zapcore.DefaultLineEnding,
			EncodeLevel:    zapcore.CapitalColorLevelEncoder,
			EncodeTime:     zapcore.ISO8601TimeEncoder,
			EncodeDuration: zapcore.SecondsDurationEncoder,
			EncodeCaller:   zapcore.ShortCallerEncoder,
		},
		OutputPaths:      []string{"stdout"},
		ErrorOutputPaths: []string{"stdout"},
	}
	if Version == "dev" {
		config.EncoderConfig.CallerKey = "caller"
	}
	logger, err := config.Build()
	if err != nil {
		zap.S().Fatal(err)
	}
	logger = logger.Named("markdown-renderer")
	zap.ReplaceGlobals(logger)
}

func getHttpFile(w http.ResponseWriter, r *http.Request) ([]byte, error) {
	clientIP := realip.FromRequest(r)
	md := *dataServerUrl + strings.Replace(r.URL.Path, ".md", "", -1) + ".md"
	log.Printf("try to get " + md)
	resp, err := http.Get(md)
	if err != nil {
		msg := fmt.Sprintf("[markdown-renderer] getHttpFile(): client:%s, path:%s, err:%s", clientIP, err.Error(), r.URL.Path)
		http.Error(w, msg, http.StatusInternalServerError)
		return nil, err
	}
	if resp.StatusCode != 200 {
		msg := fmt.Sprintf("[markdown-renderer] getHttpFile(): client:%s, path:%s, statusCode:%d", clientIP, resp.StatusCode, r.URL.Path)
		http.Error(w, msg, http.StatusInternalServerError)
		return nil, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	return body, err
}

func getLocalFile(w http.ResponseWriter, r *http.Request) ([]byte, error) {
	clientIP := realip.FromRequest(r)
	ext := filepath.Ext(r.URL.Path)
	if ext != ".md" && ext != ".markdown" {
		err := fmt.Errorf("[markdown-renderer]getLocalFile(): client:%s, path:%s, invalid extension: %s", clientIP, r.URL.Path, ext)
		return nil, err
	}
	md := strings.Replace(r.URL.Path, "..", "", -1)
	mdPath := *dataRoot + md
	zap.S().Infof("[markdown-renderer]getLocalFile(): client:%s, reading file: %s", clientIP, md)
	body, err := ioutil.ReadFile(mdPath)
	if err != nil {
		e := fmt.Errorf("[markdown-renderer] getLocalFile(): client:%s, failed to get content of: %s, err: %w", clientIP, md, err)
		return nil, e
	}
	return body, nil
}

func renderMarkdownHandler(w http.ResponseWriter, r *http.Request) {
	clientIP := realip.FromRequest(r)
	w.Header().Set("Content-Type", "text/html;charset=UTF-8")
	m := validPath.FindStringSubmatch(r.URL.Path)
	if m == nil {
		e := fmt.Sprintf("[markdown-renderer] client:%s, not a valid path: %s", clientIP, r.URL.Path)
		zap.S().Error(e)
		http.Error(w, e, http.StatusNotAcceptable)
		return
	}

	var body []byte
	var err error
	if *mode == "http" {
		body, err = getHttpFile(w, r)
	} else {
		body, err = getLocalFile(w, r)
	}

	if err != nil {
		zap.S().Error(err)
		w.Write([]byte("<pre>markdown-renderer err:\n" + err.Error() + "</pre>"))
		return
	}

	bo := bufio.NewWriter(w)
	bo.WriteString(fmt.Sprintf(`
<!--
-------------------------------------
 _   _                   ____  __  __
| \ | | __ _ _ __   ___ |  _ \|  \/  |
|  \| |/ _' | "_ \ / _ \| | | | |\/| |
| |\  | (_| | | | | (_) | |_| | |  | |
|_| \_|\__,_|_| |_|\___/|____/|_|  |_|

Brought to you by 荒野無燈@nanodm.net
We gratefully accept donations at:
https://nanodm.net/donate

-------------------------------------
-->
<html>
<head>
<meta name="referrer" content="no-referrer"/>
<meta charset="utf-8">
<meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name=viewport>
<title>%s - NanoDM Markdown Render</title>
<link href="/assets/gfm.css" media="all" rel="stylesheet" type="text/css" />
<link href="//cdn.bootcss.com/octicons/2.1.2/octicons.min.css" media="all" rel="stylesheet" type="text/css" />
</head>
<body>
<article class="markdown-body entry-content" style="padding: 30px;">`, filepath.Base(r.URL.Path)))
	bo.Write(github_flavored_markdown.Markdown(body))
	bo.WriteString(`</article></body></html>`)
	bo.Flush()
}

func main() {
	log.SetOutput(os.Stdout)
	flag.Parse()
	http.HandleFunc("/", renderMarkdownHandler)
	// Serve the "/assets/gfm.css" file.
	http.Handle("/assets/", http.StripPrefix("/assets", gfmstyle.AssetsHandler()))

	data := *dataRoot
	if (*mode == "http") {
		data = *dataServerUrl
	}

	zap.S().Infof("[markdown-renderer] server %s started. listen addr: %s, work mode: %s, data source: %s\n", Version, *addr, *mode, data)
	e := http.ListenAndServe(*addr, nil)
	if e != nil {
		zap.S().Fatal("[markdown-renderer] ListenAndServe err:%s", e)
	}
}
