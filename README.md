# Markdown Renderer

Markdown Renderer is a very simple HTTP server written in Go.  It
renders Markdown documents retrieved from another (specified) HTTP
server into HTML.

Markdown Renderer uses package
[`github.com/shurcooL/github_flavored_markdown`](https://github.com/shurcooL/github_flavored_markdown)
to render Markdown documents.

Package github_flavored_markdown provides a GitHub Flavored Markdown renderer with fenced code block highlighting,
clickable heading anchor links.

# Usage

use local source:

```
./markdown-renderer -mode local -root /data/www
```

use remote source:
```
./markdown-renderer -mode http -root -dataserver=http://example.com:8083
```

# nginx
```
        location ~ \.(?:md|markdown)$$ {
                proxy_pass http://192.168.6.11:8002; # Markdown Renderer server.
        }
        location =/assets/gfm.css {
              proxy_pass http://192.168.6.11:8002; # Markdown Renderer server.
        }
```